class Sorter {

	printSortByItem(data) {
		data.sort((a, b) => {
			if (a.item < b.item) return -1
			return a.item > b.item ? 1 : 0
		})

		console.log('Sorted by item name:', data);
	}

	printSortByTotalPrice(data) {
		let itemsTotals = Counter.getEachRecordTotalCost([...data])

		itemsTotals.sort((a, b) => {
			if (a.totals < b.totals) return -1
			return a.totals > b.totals ? 1 : 0
		})

		console.log('Sorted by item total price:', itemsTotals);
	}

	printCheapestOrange(data) {
		let cheapestOrangePrice = 0;
		let cheapestOrangeType = '';

		data.forEach(function (item) {
			if (item.item === 'orange') {
				let currentItemPrice = PriceFormatter.getPriceAmount(item.pricePerKilo);

				if (cheapestOrangePrice === 0) {
					cheapestOrangePrice = currentItemPrice;
					cheapestOrangeType = item.type;
				} else if (cheapestOrangePrice > currentItemPrice) {
					cheapestOrangePrice = currentItemPrice;
					cheapestOrangeType = item.type;
				}
			}
		})

		console.log('Cheapest orange type is ' + cheapestOrangeType);
		console.log('Cheapest orange price is ' + cheapestOrangePrice);
	}
}
