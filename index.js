let inputData = [
	{"item": "apple", "type": "Fuji", "weight": 10, "pricePerKilo": "$3"},
	{"item": "orange", "type": "Clementine", "weight": 6, "pricePerKilo": "$7"},
	{"item": "watermelon", "type": "Nova", "quantity": 1, "pricePerItem": "$5"},
	{"item": "orange", "type": "Navel", "weight": 6, "pricePerKilo": "$7"},
	{"item": "pineapple", "type": "Queen", "quantity": 4, "pricePerItem": "$15"},
	{"item": "pineapple", "type": "Pernambuco", "quantity": 3, "pricePerItem": "$12"},
	{"item": "apple", "type": "Cameo", "weight": 6, "pricePerKilo": "$7"},
	{"item": "watermelon", "type": "Trio", "quantity": 2, "pricePerItem": "$9"},
	{"item": "pineapple", "type": "Red Spanish", "quantity": 3, "pricePerItem": "$9,99"},
	{"item": "watermelon", "type": "Millionaire", "quantity": 2, "pricePerItem": "$7"},
	{"item": "orange", "type": "Tangerine", "weight": 4, "pricePerKilo": "$4,99"},
	{"item": "apple", "type": "Jazz", "weight": 4, "pricePerKilo": "$5"},
];

//Validate the data according to the following rules:
// item: string,
// type: string,
// weight: number,
// quantity: number,
// pricePerKilo: `“$” + number` - string,
// pricePerItem: `“$” + number` - string
let validator = new Validator();
validator.validate(inputData);

let counter = new Counter();

//Print to the console the total quantity of all watermelons (`Watermelons - ${quantity}`);
counter.printWatermelonsTotalQuantity(inputData);
//Print to the console the total weight of all apples (`Apples - ${weight}`);
counter.printApplesTotalWeight(inputData);

let sorter = new Sorter();
//Sort the array in alphabetical order by item field and print it to the console;
sorter.printSortByItem(inputData);

// Sort the array by cost of the record and print it to the console;
sorter.printSortByTotalPrice(inputData);

//Print to the terminal the type of oranges with the least price (`The cheapest orange type is: ${type}`);
sorter.printCheapestOrange(inputData);

//Print to the console the cost of the goods by item name (
// `Apples - ${costApples},
// Pineapples - ${costPineapples},
// Watermelons - ${costWatermelons},
// Oranges - ${costOranges}`);
counter.printEachRecordTotalTypeCost(inputData);

//Print to the console the result of the execution of this function
counter.printTotalAmount(inputData);
