class Validator {

	validate(data) {
		if (!Array.isArray(data)) {
			throw new Error('Input data should be an array.')
		}

		data.forEach(function (item) {
			validator.validateScalar(item.item, 'Item property should be string!', 'string');
			validator.validateScalar(item.type, 'Type property should be string!', 'string');

			if (item.hasOwnProperty('weight')) {
				validator.validateScalar(item.weight, 'Weight property should be number!', 'number');
			}

			if (item.hasOwnProperty('quantity')) {
				validator.validateScalar(item.quantity, 'Quantity property should be number!', 'number');
			}

			if (item.hasOwnProperty('pricePerItem')) {
				validator.validatePrice(item.pricePerItem, 'Price per item')
			}

			if (item.hasOwnProperty('pricePerKilo')) {
				validator.validatePrice(item.pricePerKilo, 'Price per kilo')
			}
		});
	}

	validateScalar(propertyData, message, requiredType) {
		if (typeof propertyData !== requiredType) {
			throw new Error(message)
		}
	}

	validatePrice(priceRaw, validatedProperty) {
		validator.validateScalar(priceRaw, validatedProperty + ' property should be string!', 'string');
		let dollarSign = priceRaw.substr(0, 1);

		if (dollarSign !== '$') {
			throw new Error(validatedProperty + ' should start with $ symbol.')
		}

		if (isNaN(PriceFormatter.getPriceAmount(priceRaw))) {
			throw new Error(validatedProperty + ' should be valid number.')
		}
	}
}
