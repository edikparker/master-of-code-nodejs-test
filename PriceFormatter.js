class PriceFormatter {

	static getPriceAmount(priceRaw) {
		let price = priceRaw.substr(1);

		return Number(price.replace(',', '.'));
	}
}
