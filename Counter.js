class Counter {

	printWatermelonsTotalQuantity(data) {
		let quantity = 0;

		data.forEach(function (item) {
			if (item.item === 'watermelon') {
				quantity += item.quantity;
			}
		});

		console.log('Watermelons - ' + quantity);
	}

	printApplesTotalWeight(data) {
		let applesTotalWeight = 0;

		data.forEach(function (item) {
			if (item.item === 'apple') {
				applesTotalWeight += item.weight;
			}
		});

		console.log('Apples - ' + applesTotalWeight);
	}

	printEachRecordTotalTypeCost(data) {
		let outputRaw = counter.getEachTypePriceAmount(data);
		let formattedOutput = [];

		for (let name in outputRaw) {
			  formattedOutput.push(name[0].toUpperCase() + name.slice(1) + 's - ' + outputRaw[name].toFixed(2));
		}

		console.log(formattedOutput);
	}

	printTotalAmount(data) {
		let outputRaw = counter.getEachTypePriceAmount(data);
		let totalSum = 0;

		for (let name in outputRaw) {
			totalSum+= Number(outputRaw[name].toFixed(2));
		}

		console.log('Total sum is ' + totalSum.toFixed(2));
	}

	getEachTypePriceAmount(data) {
		let outputRaw = [];

		data.forEach(function (item) {
			if (typeof outputRaw[item.item] === "undefined") {
				outputRaw[item.item.toString()] = 0;
			}

			if (item.hasOwnProperty('weight') && item.hasOwnProperty('pricePerKilo')) {
				outputRaw[item.item.toString()] += item.weight * PriceFormatter.getPriceAmount(item.pricePerKilo);
			}

			if (item.hasOwnProperty('quantity') && item.hasOwnProperty('pricePerItem')) {
				outputRaw[item.item.toString()] += item.quantity * PriceFormatter.getPriceAmount(item.pricePerItem);
			}
		});

		return outputRaw;
	}

	static getEachRecordTotalCost(data) {
		data.forEach(function (item) {
			if (item.hasOwnProperty('weight') && item.hasOwnProperty('pricePerKilo')) {
				item.totals = item.weight * PriceFormatter.getPriceAmount(item.pricePerKilo);
			}

			if (item.hasOwnProperty('quantity') && item.hasOwnProperty('pricePerItem')) {
				item.totals = item.quantity * PriceFormatter.getPriceAmount(item.pricePerItem);
			}
		});

		return data;
	}
}
